from django.http import HttpResponse
from django.db import connection
from django.views import View
from collections import defaultdict
import json


class VectorApiEndPoint(View):
    
    # Coordinate system used for the data returned to a client
    __COORD_SYSTEM_SRID = 900913
    __MAX_OFFSET = 2000
    __ALLOWED_TAGS = ['building','highway','landuse', 'natural', 'waterway']

    def post(self, request):
        """
        Generate GeoJSON response for a request. 
        The coordinate point in the request should be in EPSG:4326.
        The response contains geometries extracted from the db around the point
        within some radius (offset parameter).
        """
        
        # For testing purposes:
        """
        lon = 21.525356
        lat = 61.130697
        offset = 1000;
        """
        
        # Extract parameters from the request body
        try:
            params = self.DecodeJsonRequest(request)
        except (ValueError, KeyError):
            return HttpResponse(
                'Invalid parameters/JSON. Required coordinate \
                system EPSG {0}, lon: [-180.0, 180.0], lat: [-90.0, 90.0], \
                offset: [0, {1}], allowed tags: {2}. Example request: \
                {{"lon":"21.525356", "lat":"61.130697", "offset":"1000", \
                "tags":["landuse","highway"]}}'
                    .format(self.__COORD_SYSTEM_SRID, self.__MAX_OFFSET, \
                            self.__ALLOWED_TAGS) )
        
        dataFromDb = self.FetchDataFromDb(params);
        geoJsonMsg = self.GenerateJsonResponse(dataFromDb, params)
        
        return HttpResponse(geoJsonMsg, content_type='application/json' )
    
    
    def DecodeJsonRequest(self, request):
        """
        Extract parameters from the request body.
        """
        
        bodyUnicode = request.body.decode('utf-8')
        jsonRequest = json.loads(bodyUnicode)
        
        # Parse strings to numbers
        lon = float(jsonRequest['lon'])
        lat = float(jsonRequest['lat'])
        offset = int(jsonRequest['offset'])
        tags = jsonRequest['tags']
        
        # Check that tags in the request are allowed (to prevent sql-injection)
        for tag in tags:
            if not tag in self.__ALLOWED_TAGS:
                raise ValueError("Tags not allowed.")
        
        # Check that tags in the request are not empty
        if len(tags) == 0:
            raise ValueError
        
        # Check that parameters are within a valid range
        if not(-180 <= lon <= 180) or not(-90 <= lat <= 90) or \
                    not(0 <= offset <= self.__MAX_OFFSET):
            raise ValueError("Parameters out of the valid range")

        return {'lon':lon, 'lat':lat, 'offset':offset, 'tags':tags}
    
    
    def FetchDataFromDb(self, params):
        """
        Fetch geometries around some point (lon,lat, EPSG:4326) from the 
        database within an offset defined in metres.
        """
        
        # Generate SQL for tags to be extracted from the db
        tableNames = ['planet_osm_line', 'planet_osm_point', 'planet_osm_polygon']
        tags = params['tags']
        tagsSQL = ''
        numTags = len(tags)
        for i in range(numTags):
            tagsSQL = tagsSQL + 'exist(tags, \'{}\')'.format(tags[i])
            
            if i+1 < numTags:   # Add "OR" if not last round
                tagsSQL = tagsSQL + ' OR '
        
        # Generate the final SQL command.
        sql2Exec = {}
        for tableName in tableNames:
            """
            sql2Exec[tableName] = '\
                SELECT \
                ST_AsGeoJSON((St_Dump(St_Intersection(ST_Buffer(ST_Transform( \
                    ST_GeomFromText(\'POINT(%(lon)s %(lat)s)\',4326),900913), %(offset)s), geom))).geom), \
                tags, ST_Xmin(geom), ST_Ymin(geom), ST_Xmax(geom), ST_Ymax(geom) \
                FROM \
                \
                (SELECT way As geom, HSTORE_TO_JSON(tags) As tags \
                FROM osm.{0} WHERE \
                ( {1} ) \
                AND \
                ST_DWithin(\
                    way, \
                    ST_Transform( \
                        ST_GeomFromText( \'POINT(%(lon)s %(lat)s)\',4326), \
                            900913),\
                    %(offset)s)) As Asdf;'.format(tableName, tagsSQL)
            """ 
            
            sql2Exec[tableName] = '\
                SELECT ST_AsGeoJSON(clippedGeom), tags, \
                ST_Xmin(clippedGeom), ST_Ymin(clippedGeom), ST_Xmax(clippedGeom), ST_Ymax(clippedGeom) \
                FROM \
                 \
                (SELECT \
                tags, (St_Dump(St_Intersection(ST_Buffer(ST_Transform( \
                    ST_GeomFromText(\'POINT(%(lon)s %(lat)s)\',4326),900913), %(offset)s), geom))).geom AS clippedGeom \
                 \
                FROM \
                \
                (SELECT way As geom, HSTORE_TO_JSON(tags) As tags \
                FROM osm.{0} WHERE \
                ( {1} ) \
                AND \
                ST_DWithin(\
                    way, \
                    ST_Transform( \
                        ST_GeomFromText( \'POINT(%(lon)s %(lat)s)\',4326), \
                            900913),\
                    %(offset)s)) As Asdf) As Asdf2;'.format(tableName, tagsSQL)
            
        
        
        # Fetch data from DB
        dataFromDb = {}
        with connection.cursor() as cursor:
                
            cursor.execute(sql2Exec['planet_osm_polygon'], params)
            dataFromDb['planet_osm_polygon'] = cursor.fetchall()
            cursor.execute(sql2Exec['planet_osm_line'], params)
            dataFromDb['planet_osm_line'] = cursor.fetchall()
            cursor.execute(sql2Exec['planet_osm_point'], params)
            dataFromDb['planet_osm_point'] = cursor.fetchall()
        
        return dataFromDb


    def GenerateJsonResponse(self, dataFromDb, params):
        """
        Generate string (in geoJSON format) from the geometries fetched from the
        database.
        """
        
        # Create JSON for geometries and properties. Sort by tag.
        tags = params['tags']
        GeometriesSortedByTag = {}   # JSON-object for each tag. Requested geometries
                                # are stored here
        BBoxesForGeomsWithTag = {}
        for table in dataFromDb:
            data = dataFromDb[table]    # Data from a certain database table e.g. planet_osm_polygon
            numRows = len(data)
            
            for i in range(numRows):
                geom = data[i][0]
                properties = data[i][1]
                geomXMin = data[i][2]
                geomYMin = data[i][3]
                geomXMax = data[i][4]
                geomYMax = data[i][5]   # Bounding box for the geometry
                
                # Sort geometries according to tags
                for tag in tags:
                    if tag in properties:   # Pick corresponding tag from properties
                        
                        # If a JSON-object for the tag is not yet initialized 
                        if not tag in GeometriesSortedByTag:
                            GeometriesSortedByTag[tag] = '"' + tag + '": \
                                { "type":"FeatureCollection", "features": [ '
                            tmpString = '{ "type":"Feature", "geometry":' + \
                                geom + ','
                            tmpString = tmpString + '"properties":' + \
                                json.dumps(properties) + '}'
                            GeometriesSortedByTag[tag] = GeometriesSortedByTag[tag] \
                                + tmpString
                        else:   #  If already initialized
                            tmpString = ',{ "type":"Feature", "geometry":' + \
                                geom + ','
                            tmpString = tmpString + '"properties":' + \
                                json.dumps(properties) + '}'
                            GeometriesSortedByTag[tag] = GeometriesSortedByTag[tag] \
                                + tmpString
                        
                        # Find bounding box for geometries with a certain tag      
                        if BBoxesForGeomsWithTag.get(tag) == None:  # If empty
                            BBoxesForGeomsWithTag[tag] = {"XMin": geomXMin, \
                                                          "YMin": geomYMin, \
                                                          "XMax": geomXMax, \
                                                          "YMax": geomYMax}
                        if geomXMin < BBoxesForGeomsWithTag[tag]["XMin"]:
                            BBoxesForGeomsWithTag[tag]["XMin"] = geomXMin
                        if geomYMin < BBoxesForGeomsWithTag[tag]["YMin"]:
                            BBoxesForGeomsWithTag[tag]["YMin"] = geomYMin
                        if geomXMax > BBoxesForGeomsWithTag[tag]["XMax"]:
                            BBoxesForGeomsWithTag[tag]["XMax"] = geomXMax
                        if geomYMax > BBoxesForGeomsWithTag[tag]["YMax"]:
                            BBoxesForGeomsWithTag[tag]["YMax"] = geomYMax
                    
        # Add bounding box for geometries associated with a certain tag and 
        # write closing brackets for the JSON-objects
        for tag in GeometriesSortedByTag:
            XMin = str(BBoxesForGeomsWithTag[tag]["XMin"])
            YMin = str(BBoxesForGeomsWithTag[tag]["YMin"])
            XMax = str(BBoxesForGeomsWithTag[tag]["XMax"])
            YMax = str(BBoxesForGeomsWithTag[tag]["YMax"])
            
            tmpString = '],"bbox": [' + XMin +','+ YMin +','+ XMax +','+ YMax + ']}'
            GeometriesSortedByTag[tag] = GeometriesSortedByTag[tag] +  tmpString
        
        # Create the GeoJSON message
        geoJsonMsg = '{'
        for tag in GeometriesSortedByTag:
            geoJsonMsg = geoJsonMsg + GeometriesSortedByTag[tag] + ','
        
        # Finally, add the used coordinate system
        tmpString = '"crs": {"type":"name", "properties": {"name": \
            "SRID ' + str(self.__COORD_SYSTEM_SRID) + '"}}'
        geoJsonMsg = geoJsonMsg + tmpString + ' }'
        
        return geoJsonMsg
    
    
    
    
    