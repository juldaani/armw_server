from django.test import TestCase
from django.urls import reverse
from django.db import connection
import json
import time


def PopulateTestDb():
    with connection.cursor() as cursor:
        # Create schema and tables
        cursor.execute("CREATE SCHEMA osm")
        cursor.execute("CREATE EXTENSION IF NOT EXISTS hstore")
        cursor.execute("CREATE TABLE osm.planet_osm_polygon(\
            way geometry(Geometry,900913), tags hstore );")
        cursor.execute("CREATE TABLE osm.planet_osm_line(\
            way geometry(Geometry,900913), tags hstore );")
        cursor.execute("CREATE TABLE osm.planet_osm_point(\
            way geometry(Geometry,900913), tags hstore );")
        
        # Populate tables with some data around point (21.525356 61.130697) (EPSG 
        # 4326), offset=1000. SRID of OSM-data in the database is 900913.
        # Polygons:
        cursor.execute("INSERT INTO osm.planet_osm_polygon (way, tags)\
            values (ST_GeomFromText( 'POLYGON((2395400.83 8655229.9,2395407.04 8655257.66,2395424.64 8655253.71,2395418.44 8655225.96,2395400.83 8655229.9))',900913),\
            ' ""building""=>""yes"", ""way_area""=>""513.105"" '); ")
        cursor.execute("INSERT INTO osm.planet_osm_polygon (way, tags)\
            values (ST_GeomFromText( 'POLYGON((2395550.95 8655065.36,2395553.29 8655087.59,2395614.16 8655081.13,2395611.8 8655058.91,2395550.95 8655065.36))',900913),\
            ' ""building""=>""yes"", ""way_area""=>""1367.78"" '); ")
        cursor.execute("INSERT INTO osm.planet_osm_polygon (way, tags)\
            values (ST_GeomFromText( 'POLYGON((2395840.96 8656191.49,2395853.27 8656189.32,2395862.59 8656184.32,2395867.07 8656180.33,2395867.57 8656176.34,2395865.08 8656171.84,2395848.77 8656135.41,2395846.11 8656131.91,2395843.45 8656132.92,2395842.95 8656136.57,2395840.96 8656191.49))',900913),\
            ' ""landuse""=>""grass"", ""way_area""=>""950.372"" '); ")
        # Line strings:
        cursor.execute("INSERT INTO osm.planet_osm_line (way, tags)\
            values (ST_GeomFromText( 'LINESTRING(2395452.6 8655373.21,2395451.96 8655329.01,2395446.57 8655285.6,2395435.08 8655239.21,2395427.19 8655200.78,2395425.87 8655193.33,2395411.92 8655099.04,2395412.08 8655065.15)',900913),\
            ' ""name""=>""Tullivahe"", ""highway""=>""residential"", ""name:fi""=>""Tullivahe"", ""surface""=>""unpaved"" '); ")
        cursor.execute("INSERT INTO osm.planet_osm_line (way, tags)\
            values (ST_GeomFromText( 'LINESTRING(2395544.49 8655184.55,2395512.8 8655201.1,2395491.84 8655202.33,2395467.65 8655198.71,2395427.19 8655200.78)',900913),\
            ' ""highway""=>""service"", ""service""=>""parking_aisle"", ""surface""=>""unpaved"" '); ")
        cursor.execute("INSERT INTO osm.planet_osm_line (way, tags)\
            values (ST_GeomFromText( 'LINESTRING(2395552.1 8655169.06,2395539.94 8655177.89,2395512.14 8655195.02,2395496.17 8655195.96,2395467.46 8655192.16,2395425.87 8655193.33)',900913),\
            ' ""name""=>""Vanhankirkonkatu"", ""highway""=>""footway"" '); ")
        # Points
        cursor.execute("INSERT INTO osm.planet_osm_point (way, tags)\
            values (ST_GeomFromText( 'POINT(2395539.94 8655177.89)',900913),\
            ' ""highway""=>""crossing"", ""crossing""=>""uncontrolled"" '); ")
        cursor.execute("INSERT INTO osm.planet_osm_point (way, tags)\
            values (ST_GeomFromText( 'POINT(2395643.07 8655368.85)',900913),\
            ' ""highway""=>""crossing"", ""crossing""=>""uncontrolled"" '); ")
        

class VectorApiTests(TestCase):
    
    # Coordinates for the test point
    LONGITUDE = 21.525356
    LATITUDE = 61.130697
    OFFSET = 1000;
    TAGS = ['building']
    
    def test_IsStatusCodeOk(self):
        PopulateTestDb()
        jsonRequest = {'lon':self.LONGITUDE, 'lat':self.LATITUDE, \
                       'offset':self.OFFSET, 'tags':self.TAGS}
        response = self.client.post(reverse('vector_api_url'), 
                                    json.dumps(jsonRequest),
                                    content_type="application/json")
        self.assertEqual(response.status_code, 200)
        
        
    def test_CanParseResponse2Json(self):
        PopulateTestDb()
        jsonRequest = {'lon':self.LONGITUDE, 'lat':self.LATITUDE, \
                       'offset':self.OFFSET, 'tags':self.TAGS}
        response = self.client.post(reverse('vector_api_url'), 
                                    json.dumps(jsonRequest),
                                    content_type="application/json").json()
        
        
    def test_AreBoundingBoxesCalculatedCorrectly_ArtificialData(self):
        # Populate test database
        with connection.cursor() as cursor:
            # Create schema and tables
            cursor.execute("CREATE SCHEMA osm")
            cursor.execute("CREATE EXTENSION IF NOT EXISTS hstore")
            cursor.execute("CREATE TABLE osm.planet_osm_polygon(\
                way geometry(Geometry,900913), tags hstore );")
            cursor.execute("CREATE TABLE osm.planet_osm_line(\
                way geometry(Geometry,900913), tags hstore );")
            cursor.execute("CREATE TABLE osm.planet_osm_point(\
                way geometry(Geometry,900913), tags hstore );")
            # Buildings:
            cursor.execute("INSERT INTO osm.planet_osm_polygon (way, tags)\
                values (ST_GeomFromText( 'POLYGON((1000000 1000000, 1000200 1000300, 1000500 1000200, 1000000 1000000))',900913),\
                ' ""building""=>""yes"" '); ")
            cursor.execute("INSERT INTO osm.planet_osm_polygon (way, tags)\
                values (ST_GeomFromText( 'POLYGON((1000400 1000400, 1000500 1000500, 1000600 1000400, 1000400 1000400))',900913),\
                ' ""building""=>""yes"" '); ")
        
        # Fetch correct extents for the geometries to be queried (calculated with
        # the database engine)
        with connection.cursor() as cursor:
            cursor.execute(" \
                SELECT \
                min(ST_Xmin(way)), min(ST_Ymin(way)), max(ST_Xmax(way)), max(ST_Ymax(way)) \
                FROM osm.planet_osm_polygon \
                WHERE\
                ( EXIST(tags, 'building') ) \
                AND \
                ST_DWithin( \
                way, \
                ST_Transform( ST_GeomFromText( 'POINT(1000000 1000000)',900913), 900913),\
                1000); ")
            data = cursor.fetchall()
        
        # Calculate correct bounding box
        xMinCorrect = float((data[0][0]))
        yMinCorrect = float((data[0][1]))
        xMaxCorrect = float((data[0][2]))
        yMaxCorrect = float((data[0][3]))
        
        # Make query
        jsonRequest = {'lon':8.9831528412, 'lat':8.94657385054, \
                       'offset':2000, 'tags':['building']}
        response = self.client.post(reverse('vector_api_url'), 
                                    json.dumps(jsonRequest),
                                    content_type="application/json")
        strResponse = response.content.decode('utf8')
        decodedJson = json.loads(strResponse)
        
        # Check that bounding box coordinates in the request match on correct 
        # coordinates
        BBox = decodedJson['building']['bbox']
        if float(BBox[0]) != xMinCorrect or float(BBox[1]) != yMinCorrect \
                or float(BBox[2]) != xMaxCorrect or float(BBox[3]) != yMaxCorrect:
            self.fail("Bounding box coordinates are incorrect !")
        
        
    def test_AreBoundingBoxesCalculatedCorrectly_TrueData(self):
        PopulateTestDb()
        
        # Fetch correct extents for the geometries to be queried (calculated using
        # the database engine)
        with connection.cursor() as cursor:
            cursor.execute(" \
                SELECT \
                min(ST_Xmin(way)), min(ST_Ymin(way)), max(ST_Xmax(way)), max(ST_Ymax(way)) \
                FROM osm.planet_osm_point \
                WHERE\
                ( EXIST(tags, 'highway') ) \
                AND \
                ST_DWithin( \
                way, \
                ST_Transform( ST_GeomFromText( 'POINT(21.525356 61.130697)',4326), 900913),\
                2000); ")
            dataPointsHighway = cursor.fetchall()
            cursor.execute(" \
                SELECT \
                min(ST_Xmin(way)), min(ST_Ymin(way)), max(ST_Xmax(way)), max(ST_Ymax(way)) \
                FROM osm.planet_osm_line \
                WHERE\
                ( EXIST(tags, 'highway') ) \
                AND \
                ST_DWithin( \
                way, \
                ST_Transform( ST_GeomFromText( 'POINT(21.525356 61.130697)',4326), 900913),\
                2000); ")
            dataLinesHighway = cursor.fetchall()
            cursor.execute(" \
                SELECT \
                min(ST_Xmin(way)), min(ST_Ymin(way)), max(ST_Xmax(way)), max(ST_Ymax(way)) \
                FROM osm.planet_osm_polygon \
                WHERE\
                ( EXIST(tags, 'landuse') ) \
                AND \
                ST_DWithin( \
                way, \
                ST_Transform( ST_GeomFromText( 'POINT(21.525356 61.130697)',4326), 900913),\
                2000); ")
            dataPolygsLanduse = cursor.fetchall()
        
        # Calculate correct bounding box
        xMinCorrectPointsHighway = float((dataPointsHighway[0][0]))
        yMinCorrectPointsHighway = float((dataPointsHighway[0][1]))
        xMaxCorrectPointsHighway = float((dataPointsHighway[0][2]))
        yMaxCorrectPointsHighway = float((dataPointsHighway[0][3]))
        xMinCorrectLinesHighway = float((dataLinesHighway[0][0]))
        yMinCorrectLinesHighway = float((dataLinesHighway[0][1]))
        xMaxCorrectLinesHighway = float((dataLinesHighway[0][2]))
        yMaxCorrectLinesHighway = float((dataLinesHighway[0][3]))
        xMinCorrect = min([xMinCorrectPointsHighway, xMinCorrectLinesHighway])
        yMinCorrect = min([yMinCorrectPointsHighway, yMinCorrectLinesHighway])
        xMaxCorrect = max([xMaxCorrectPointsHighway, xMaxCorrectLinesHighway])
        yMaxCorrect = max([yMaxCorrectPointsHighway, yMaxCorrectLinesHighway])
        xMinCorrectPolygsLanduse = float((dataPolygsLanduse[0][0]))
        yMinCorrectPolygsLanduse = float((dataPolygsLanduse[0][1]))
        xMaxCorrectPolygsLanduse = float((dataPolygsLanduse[0][2]))
        yMaxCorrectPolygsLanduse = float((dataPolygsLanduse[0][3]))
        
        # Make query
        jsonRequest = {'lon':self.LONGITUDE, 'lat':self.LATITUDE, \
                       'offset':2000, 'tags':['highway','landuse']}
        response = self.client.post(reverse('vector_api_url'), 
                                    json.dumps(jsonRequest),
                                    content_type="application/json")
        strResponse = response.content.decode('utf8')
        decodedJson = json.loads(strResponse)
        
        # Check that bounding box coordinates in the request match on correct 
        # coordinates
        BBoxHighway = decodedJson['highway']['bbox']
        BBoxLanduse = decodedJson['landuse']['bbox']
        
        if float(BBoxHighway[0]) != xMinCorrect or float(BBoxHighway[1]) != yMinCorrect \
                or float(BBoxHighway[2]) != xMaxCorrect or float(BBoxHighway[3]) != yMaxCorrect:
            self.fail("Bounding box coordinates are incorrect !")
            
        if float(BBoxLanduse[0]) != xMinCorrectPolygsLanduse or \
                float(BBoxLanduse[1]) != yMinCorrectPolygsLanduse or\
                float(BBoxLanduse[2]) != xMaxCorrectPolygsLanduse or\
                float(BBoxLanduse[3]) != yMaxCorrectPolygsLanduse:
            self.fail("Bounding box coordinates are incorrect !")
    
    
    def test_AreAllBuildingGeometriesPresentInResponse(self):
        PopulateTestDb()
        
        # Fetch correct number of building geometries
        with connection.cursor() as cursor:
            cursor.execute(" \
                SELECT COUNT(way)\
                FROM osm.planet_osm_polygon\
                WHERE\
                ( EXIST(tags, 'building') ) \
                AND\
                ST_DWithin(\
                    way,\
                    ST_Transform( ST_GeomFromText( 'POINT(21.525356 61.130697)',4326), 900913),\
                    1000) ")
            data = cursor.fetchall()
        numGeoms = int(data[0][0])
            
        # Make query
        jsonRequest = {'lon':self.LONGITUDE, 'lat':self.LATITUDE, \
                       'offset':2000, 'tags':['building']}
        response = self.client.post(reverse('vector_api_url'), 
                                    json.dumps(jsonRequest),
                                    content_type="application/json")
        strResponse = response.content.decode('utf8')
        decodedJson = json.loads(strResponse)
        
        # Check that bounding box coordinates in the request match on correct 
        # coordinates
        buildingGeoms = decodedJson['building']['features']
        
        self.assertEqual(len(buildingGeoms), numGeoms)

    
    def test_AreAllLanduseGeometriesPresentInResponse(self):
        PopulateTestDb()
        
        # Fetch correct number of building geometries
        with connection.cursor() as cursor:
            cursor.execute(" \
                SELECT COUNT(way)\
                FROM osm.planet_osm_polygon\
                WHERE\
                ( EXIST(tags, 'landuse') ) \
                AND\
                ST_DWithin(\
                    way,\
                    ST_Transform( ST_GeomFromText( 'POINT(21.525356 61.130697)',4326), 900913),\
                    1000) ")
            data = cursor.fetchall()
        numGeoms = int(data[0][0])
            
        # Make query
        jsonRequest = {'lon':self.LONGITUDE, 'lat':self.LATITUDE, \
                       'offset':self.OFFSET, 'tags':['landuse']}
        response = self.client.post(reverse('vector_api_url'), 
                                    json.dumps(jsonRequest),
                                    content_type="application/json")
        strResponse = response.content.decode('utf8')
        decodedJson = json.loads(strResponse)
        
        # Check that bounding box coordinates in the request match on correct 
        # coordinates
        buildingGeoms = decodedJson['landuse']['features']
        
        self.assertEqual(len(buildingGeoms), numGeoms)
        
        
    def test_AreAllHighwayGeometriesPresentInResponse(self):
        PopulateTestDb()
        
        # Fetch correct number of building geometries
        with connection.cursor() as cursor:
            cursor.execute(" \
                SELECT COUNT(way)\
                FROM osm.planet_osm_line\
                WHERE\
                ( EXIST(tags, 'highway') ) \
                AND\
                ST_DWithin(\
                    way,\
                    ST_Transform( ST_GeomFromText( 'POINT(21.525356 61.130697)',4326), 900913),\
                    1000) ")
            data = cursor.fetchall()
        numLines = int(data[0][0])
        with connection.cursor() as cursor:
            cursor.execute(" \
                SELECT COUNT(way)\
                FROM osm.planet_osm_point\
                WHERE\
                ( EXIST(tags, 'highway') ) \
                AND\
                ST_DWithin(\
                    way,\
                    ST_Transform( ST_GeomFromText( 'POINT(21.525356 61.130697)',4326), 900913),\
                    1000) ")
            data = cursor.fetchall()
        numPoints = int(data[0][0])
            
        # Make query
        jsonRequest = {'lon':self.LONGITUDE, 'lat':self.LATITUDE, \
                       'offset':self.OFFSET, 'tags':['highway']}
        response = self.client.post(reverse('vector_api_url'), 
                                    json.dumps(jsonRequest),
                                    content_type="application/json")
        strResponse = response.content.decode('utf8')
        decodedJson = json.loads(strResponse)
        
        # Check that bounding box coordinates in the request match on correct 
        # coordinates
        highwayGeoms = decodedJson['highway']['features']
        
        self.assertEqual(len(highwayGeoms), numLines+numPoints)
        
    
    """
    Kirjoita testi testaamaan parametrien arvoalueita kun käytettävä
    projektio varmistuu.
    """
    
    def test_AreErrorsHandledIfJsonRequestFormatIsInvalid(self):
        jsonRequest = 'lat:"10", lon=22}'
        try:
            self.client.post(reverse('vector_api_url'), 
                             jsonRequest,
                             content_type="application/json")
        except Exception:
            self.fail("Errors not handled when request is not proper JSON.")
    
    def test_AreErrorsHandledIfRequestParametersAreInvalid(self):
        jsonRequest = {'lon':'g4s55', 'lat':'10.0', 'offset':233, \
            'tags':["buildi4ng"]}
        try:
            self.client.post(reverse('vector_api_url'), 
                             json.dumps(jsonRequest),
                             content_type="application/json")
        except Exception:
            self.fail("Errors not handled when some parameters in the \
                    request are invalid.")
    
    def test_AreErrorsHandledIfSomeRequestParametersAreMissing(self):
        jsonRequest = {'lon':'55.0'}
        try:
            self.client.post(reverse('vector_api_url'), 
                             json.dumps(jsonRequest),
                             content_type="application/json")
        except Exception:
            self.fail("Errors not handled when some parameters in the \
                    request are missing.")
    
    def test_AreErrorsHandledIfTagParametersAreEmpty(self):
        jsonRequest = {'lon':self.LONGITUDE, 'lat':self.LATITUDE, \
                       'offset':self.OFFSET, 'tags':[]}
        try:
            self.client.post(reverse('vector_api_url'), 
                             json.dumps(jsonRequest),
                             content_type="application/json")
        except Exception:
            self.fail("Errors not handled if tag parameters are empty. ")    

    def test_CanInputTagsWhichAreNotSafe(self):
        PopulateTestDb()
        jsonRequest = {'lon':self.LONGITUDE, 'lat':self.LATITUDE, \
                       'offset':self.OFFSET, 'tags':['; DROP TABLE osm.planet_osm_point ;']}
        
        try:
            self.client.post(reverse('vector_api_url'), 
                                    json.dumps(jsonRequest),
                                    content_type="application/json").json()
        except Exception:
            pass    #It is expected that received response is an error message 
                    #which is not json (cannot parse to json -> exception -> pass)
        else:
            self.fail("Tags which are not safe are allowed in the request \
                (sql-injection may be possible)")
        
        
