from django.apps import AppConfig


class GeodjangoConfig(AppConfig):
    name = 'vector_api'
