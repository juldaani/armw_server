from django.conf.urls import url
from .views import VectorApiEndPoint

from . import views

urlpatterns = [
    url(r'^$', VectorApiEndPoint.as_view(), name='vector_api_url' ),
]